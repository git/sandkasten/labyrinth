export const RUNNERS = ["bash", "moshell", "bun", "typescript"] as const;
const ALLOWED_LANGUAGES = new Set<string>(RUNNERS);
const aliases: Record<string, (typeof RUNNERS)[number]> = {
  JavaScript: "bun",
  TypeScript: "typescript",
};
export const IMAGES = {
  logo: "logo.png",
  background: "background.png",
};

/**
 * Prepares a buffer to be sent to the runner.
 *
 * @param jobId The job unique identifier.
 * @param code The code to be executed.
 * @param image The image to be used.
 */
export function allocateBuffer(
  jobId: string,
  code: string,
  image: string,
): Buffer {
  let cur = 0;
  const buffer = Buffer.allocUnsafe(
    jobId.length + image.length + code.length + 9,
  );
  cur = buffer.writeUInt8(0, cur);
  cur += buffer.write(jobId, cur);
  cur = buffer.writeUInt32BE(image.length, cur);
  cur = buffer.writeUInt32BE(code.length, cur);
  cur += buffer.write(image, cur);
  buffer.write(code, cur);
  return buffer;
}

export function getRunner(language: string): (typeof RUNNERS)[number] | null {
  language = aliases[language] || language.toLowerCase();
  if (ALLOWED_LANGUAGES.has(language)) {
    return language as (typeof RUNNERS)[number];
  }
  return null;
}
