import fs from "fs";
import sqlite3 from "sqlite3";

const dbDirectory = "./src/db";
const dbFilePath = `${dbDirectory}/database.db`;

export type error = {
  errno: number;
  code: string;
};

/* Fonction pour exécuter une requête sur la base de données */

/* Fonction pour exécuter une requête de modification de la base de données (INSERT, UPDATE, DELETE) */
export function runDB(
  db: sqlite3.Database,
  query: string,
  params: any[],
): Promise<void> {
  return new Promise((resolve, reject) => {
    db.run(query, params, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

/* Fonction pour récupérer plusieurs lignes de la base de données */
export function allDB<T>(
  db: sqlite3.Database,
  query: string,
  params?: any[]
): Promise<unknown[]> {
  return new Promise((resolve, reject) => {
    db.all(query, params, (err, rows) => {
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
}

/* Fonction pour récupérer une seule ligne de la base de données */
export function getDB<T>(
  db: sqlite3.Database,
  query: string,
  params: any[],
): Promise<T> {
  return new Promise((resolve, reject) => {
    db.get(query, params, (err, row: T) => {
      if (err) {
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
}

/* Fonctions pour la gestion de la base de données */

/* Créer le répertoire db s'il n'existe pas */
export function createDbDirectory() {
  if (!fs.existsSync(dbDirectory)) {
    fs.mkdirSync(dbDirectory);
  }
}

/* Ouvrir la base de données */
export function openDatabase() {
  console.log("Ouverture de la connexion à la base de données.");
  return new sqlite3.Database(
    dbFilePath,
    sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,
    (err: Error | null) => {
      if (err) console.error(err.message);
    },
  );
}

/* Fermer la base de données */
export function closeDatabase(db: sqlite3.Database) {
  db.close((err) => {
    if (err) {
      console.error(err.message);
    }
    console.log("Fermeture de la connexion à la base de données.");
  });
}

/* Create all the tables in the database */
export function createTables(db: sqlite3.Database) {
  createRegisteredUserTable(db);
  createWorkTable(db);
}

/////////////////////////// Gestion des utilisateurs ///////////////////////////
// CREATE TABLE registered_user (
//      id_user SERIAL PRIMARY KEY,
//      login VARCHAR(64) NOT NULL,
//      email VARCHAR(64) NOT NULL,
//      password VARCHAR(72) NOT NULL,
//      permissions INT NOT NULL,
//      UNIQUE (login)
// );

/* Créer la table registered_user dans la base de données */
export function createRegisteredUserTable(db: sqlite3.Database): Promise<void> {
  const tableRegisteredUser = `CREATE TABLE IF NOT EXISTS registered_user (
    id_user INTEGER PRIMARY KEY AUTOINCREMENT,
    login TEXT NOT NULL, 
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    permissions INTEGER NOT NULL,
    UNIQUE (login))`;

  return runDB(db, tableRegisteredUser, []);
}

/* Insérer un utilisateur dans la table registered_user */
export async function insertUser(
  db: sqlite3.Database,
  login: string,
  email: string,
  password: string,
  permissions: number,
): Promise<boolean> {
  const insertUserQuery = `INSERT INTO registered_user (login, email, password, permissions) VALUES (?, ?, ?, ?)`;
  try {
    await runDB(db, insertUserQuery, [login, email, password, permissions]);
    return true;
  } catch (e) {
    const error = e as error;
    if (error.code === "SQLITE_CONSTRAINT") {
      return false;
    } else {
      throw e;
    }
  }
}

/* Vérifier si un utilisateur existe dans la table registered_user */
export async function verifyUser(
  db: sqlite3.Database,
  login: string,
): Promise<User | null> {
  const verifyUserQuery = `SELECT id_user, login, password FROM registered_user WHERE login = ?`;

  const res = await getDB<User>(db, verifyUserQuery, [login]);

  if (!res) {
    return null;
  } else {
    return res;
  }
}

export type User = {
  id_user: number;
  login: string;
  password: string;
};

/* Modifier le login d'un utilisateur dans la table registered_user */
export function updateUserLogin(
  db: sqlite3.Database,
  id: number,
  newLogin: string,
) {
  const updateUserLoginQuery = `UPDATE registered_user SET login = ? WHERE id_user = ?`;

  return runDB(db, updateUserLoginQuery, [newLogin, id]);
}

/* Modifier le mot de passe d'un utilisateur dans la table registered_user */
export function updateUserPassword(
  db: sqlite3.Database,
  id: number,
  newPassword: string,
) {
  const updateUserPasswordQuery = `UPDATE registered_user SET password = ? WHERE id_user = ?`;

  return runDB(db, updateUserPasswordQuery, [newPassword, id]);
}

/* Modifier les permissions d'un utilisateur dans la table registered_user */
export function updateUserPermissions(
  db: sqlite3.Database,
  id: number,
  newPermissions: number,
) {
  const updateUserPermissionsQuery = `UPDATE registered_user SET permissions = ? WHERE id_user = ?`;

  return runDB(db, updateUserPermissionsQuery, [newPermissions, id]);
}

export function updateUserEmail(
  db: sqlite3.Database,
  id: number,
  newEmail: string
) {
  const updateUserEmailQuery = `UPDATE registered_user SET email = ? WHERE id_user = ?`;
  return runDB(db, updateUserEmailQuery, [newEmail, id]);
}

/* Supprimer un utilisateur de la table registered_user par son ID */
export function deleteUserById(db: sqlite3.Database, id: number) {
  const deleteUserQuery = `DELETE FROM registered_user WHERE id_user = ?`;

  return runDB(db, deleteUserQuery, [id]);
}

/* Supprimer un utilisateur de la table registered_user par son login */
export function deleteUserByLogin(db: sqlite3.Database, login: string) {
  const deleteUserQuery = `DELETE FROM registered_user WHERE login = ?`;

  return runDB(db, deleteUserQuery, [login]);
}

/* Supprimer tous les utilisateurs de la table registered_user */
export function deleteAllUsers(db: sqlite3.Database) {
  const deleteAllUsersQuery = `DELETE FROM registered_user`;

  return runDB(db, deleteAllUsersQuery, []);
}

/* Sélectionner tous les utilisateurs de la table registered_user */
export function selectAllUsers(db: sqlite3.Database): Promise<unknown[]> {
  const selectAllUsersQuery = `SELECT * FROM registered_user`;

  return allDB(db, selectAllUsersQuery);
}

/* Sélectionner un utilisateur par son login */
export function selectUserByLogin(db: sqlite3.Database, login: string) {
  const selectUserByLoginQuery = `SELECT * FROM registered_user WHERE login = ?`;

  return getDB(db, selectUserByLoginQuery, [login]);
}

/* Sélectionner un utilisateur par son ID */
export function selectUserById(db: sqlite3.Database, id: number) {
  const selectUserByIdQuery = `SELECT * FROM registered_user WHERE id_user = ?`;

  return getDB(db, selectUserByIdQuery, [id]);
}

/////////////////////////// Gestion des works ///////////////////////////
// CREATE TABLE work (
//     id_work SERIAL PRIMARY KEY,
//     link CHAR(36) NOT NULL,
//     user_id INT REFERENCES registered_user(id_user),
//     language TEXT NOT NULL, 
//     title VARCHAR(30) NOT NULL,
//     content TEXT NOT NULL,
//     date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
// );

/* Créer la table work dans la base de données */
export function createWorkTable(db: sqlite3.Database): Promise<void> {
  const tableWork = `CREATE TABLE IF NOT EXISTS work (
    id_work INTEGER PRIMARY KEY AUTOINCREMENT,
    link CHAR(36) NOT NULL, user_id INTEGER REFERENCES registered_user(id_user),
    language TEXT NOT NULL, 
    title TEXT NOT NULL,
    content TEXT NOT NULL, 
    date TIMESTAMP DEFAULT CURRENT_TIMESTAMP)`;
  return runDB(db, tableWork, []);
}

/* Insérer un work dans la table work */
export function insertWork(
  db: sqlite3.Database,
  link: string,
  user_id: number,
  language: string,
  title: string,
  content: string,
) {
  const insertWorkQuery = `INSERT INTO work (link, user_id, language, title, content, date) VALUES (?, ?, ?, ?, ?, ?)`;

  return runDB(db, insertWorkQuery, [link, user_id, language, title, content, new Date().toISOString()]);
}

/* Sélectionner tous les works de la table work */
export function selectAllWorks(db: sqlite3.Database): Promise<unknown[]> {
  const selectAllWorksQuery = `SELECT * FROM work`;

  return allDB(db, selectAllWorksQuery);
}

/* Sélectionner tous les works de la table work */
export function selectWorksByUserId(db: sqlite3.Database, user_id: number): Promise<unknown[]> {
  const selectWorksByUserIdQuery = `SELECT * FROM work WHERE user_id = ?`;

  return allDB(db, selectWorksByUserIdQuery, [user_id]);
}

/* Sélectionner un work par son ID */
export function selectWorkByLink(db: sqlite3.Database, link: string) {
  const selectWorkByLinkQuery = `SELECT * FROM work WHERE link = ?`;
  return getDB(db, selectWorkByLinkQuery, [link]);
}

/* Sélectionner le dernier work par l'id de l'utilisateur */
export function selectLastWorkByUserId(db: sqlite3.Database, user_id: number) {
  const selectLastWorkByUserIdQuery = `SELECT * FROM work
                                       WHERE user_id = ?
                                       ORDER BY date DESC
                                       LIMIT 1`;

  return getDB(db, selectLastWorkByUserIdQuery, [user_id]);
}

/* Supprimer tous les works de la table work */
export function deleteAllWorks(db: sqlite3.Database) {
  const deleteAllWorksQuery = `DELETE FROM work`;

  return runDB(db, deleteAllWorksQuery, []);
}

/* Supprimer un work de la table work */
export function deleteWork(db: sqlite3.Database, id: number) {
  const deleteWorkQuery = `DELETE FROM work WHERE id_work = ?`;

  return runDB(db, deleteWorkQuery, [id]);
}

/* Update the work title by its ID */
export function updateWorkTitle(
  db: sqlite3.Database,
  id: number,
  newTitle: string
) {
  const updateWorkTitleQuery = `UPDATE work SET title = ? WHERE id_work = ?`;

  return runDB(db, updateWorkTitleQuery, [newTitle, id]);
}

/* Update the work content by its ID */
export function updateWorkContent(
  db: sqlite3.Database,
  id: number,
  newContent: string,
  language: string,
) {
  const updateWorkContentQuery = `UPDATE work SET content = ?, language = ? WHERE id_work = ?`;

  return runDB(db, updateWorkContentQuery, [newContent, language, id]);
}


