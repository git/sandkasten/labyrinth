# labyrinth

The HTTP backend for the Sandkasten website.

## Compiling and executing

```bash
npm install
npm run start
```

## Endpoints

### POST /run

Runs a program and returns the output with an event stream.

The stream always defines a `id` in each event. It may be:

| id       | Description                                         |
|----------|-----------------------------------------------------|
| `stdout` | The event contains the output of the program.       |
| `stderr` | The event contains the error output of the program. |
| `exit`   | The event contains the exit code of the program.    |

## Bun support

Pending [#5019](https://github.com/oven-sh/bun/issues/5019)
