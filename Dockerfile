FROM node:22-bookworm AS runner
WORKDIR /app
ENV NODE_ENV="production"
COPY package.json package-lock.json ./
RUN npm install --omit=dev

FROM runner AS builder
COPY . .
RUN npm install && npm run build && npm prune --omit=dev

FROM runner
COPY --from=builder /app/dist /app
EXPOSE 3000
ENTRYPOINT ["node", "src/server.js"]
